FROM node:18.17.0

# Installer Cypress globalement en tant que root
RUN npm install -g cypress

# Ajouter un utilisateur non-root pour la sécurité
RUN addgroup --system --gid 1001 cypress && \
    adduser --system --uid 1001 --ingroup cypress --home /home/cypress cypress

WORKDIR /home/cypress

# Copier les fichiers du projet
COPY . .

# Installer les dépendances du projet en tant que root
RUN npm install -g npm@9.7.1

# Installer les dépendances du projet
RUN npm ci || cat /root/.npm/_logs/*-debug-0.log

# Installer wait-on en tant que root
RUN npm install --save-dev wait-on

# Créer le répertoire .npm et changer les permissions des dossiers npm et passer à l'utilisateur non-root
RUN mkdir -p /home/cypress/.npm && \
    chown -R cypress:cypress /home/cypress/.npm && \
    chown -R cypress:cypress /home/cypress

USER cypress

# Exposer le port de l'application si nécessaire (optionnel)
EXPOSE 3000

CMD ["npx", "cypress", "run"]
