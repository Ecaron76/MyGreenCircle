import { createMocks } from 'node-mocks-http';
import { GET } from '../src/app/api/groupe/route';
import { getAuthSession } from '@/lib/auth';
import { PrismaClient } from '@prisma/client';
import { NextResponse } from 'next/server';

jest.mock('@prisma/client', () => {
  const mPrismaClient = {
    group: {
      findMany: jest.fn(),
    },
  };
  return { PrismaClient: jest.fn(() => mPrismaClient) };
});

jest.mock('@/lib/auth', () => ({
  getAuthSession: jest.fn(),
}));

jest.mock('next/server', () => ({
  NextResponse: {
    json: jest.fn((data, init) => {
      return {
        status: init.status,
        json: async () => data,
      };
    }),
  },
}));

const prisma = new PrismaClient();

describe('Group Retrieval API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('GET /groups', () => {
    it('should return 403 if the user is not authenticated', async () => {
      getAuthSession.mockResolvedValue(null);

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(403);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Not Authenticated' },
        { status: 403 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Not Authenticated' });
    });

    it('should return groups if the user is authenticated', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });

      prisma.group.findMany.mockResolvedValue([
        {
          groupId: 1,
          groupName: 'Group 1',
          groupDescription: 'Description for Group 1',
          groupLocation: 'Location 1',
        },
        {
          groupId: 2,
          groupName: 'Group 2',
          groupDescription: 'Description for Group 2',
          groupLocation: 'Location 2',
        },
      ]);

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(200);
      expect(NextResponse.json).toHaveBeenCalledWith(
        expect.any(Array),
        { status: 200 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject([
        {
          groupId: 1,
          groupName: 'Group 1',
          groupDescription: 'Description for Group 1',
          groupLocation: 'Location 1',
        },
        {
          groupId: 2,
          groupName: 'Group 2',
          groupDescription: 'Description for Group 2',
          groupLocation: 'Location 2',
        },
      ]);
    });

    it('should return 500 if an error occurs', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });
      prisma.group.findMany.mockRejectedValue(new Error('Internal Server Error'));

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(500);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Something went wrong' },
        { status: 500 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Something went wrong' });
    });
  });
});
