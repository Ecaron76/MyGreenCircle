import { createMocks } from 'node-mocks-http';
import { POST, DELETE } from '../src/app/api/likes/route';

jest.mock('@prisma/client', () => {
  const mPrismaClient = {
    like: {
      findUnique: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
    },
  };
  return { PrismaClient: jest.fn(() => mPrismaClient) };
});

jest.mock('@/lib/auth', () => ({
  getAuthSession: jest.fn(),
}));

jest.mock('next/server', () => ({
  NextResponse: {
    json: jest.fn((data, init) => {
      return {
        status: init.status,
        json: async () => data,
      };
    }),
  },
}));

const { getAuthSession } = require('@/lib/auth');
const { PrismaClient } = require('@prisma/client');
const { NextResponse } = require('next/server');
const prisma = new PrismaClient();

describe('Likes API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('POST /likes', () => {
    it('should return 403 if the user is not authenticated', async () => {
      getAuthSession.mockResolvedValue(null);

      const { req, res } = createMocks({
        method: 'POST',
        body: { postId: 1 },
      });

      const response = await POST(req, res);

      expect(response.status).toBe(403);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Not Authenticated' },
        { status: 403 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Not Authenticated' });
    });

    // it('should return 400 if the user has already liked the post', async () => {
    //   getAuthSession.mockResolvedValue({ user: { id: 'user-id' } });
    //   prisma.like.findUnique.mockResolvedValue({ id: 'existing-like-id' });

    //   const { req, res } = createMocks({
    //     method: 'POST',
    //     body: { postId: 1 },
    //   });

    //   const response = await POST(req, res);

    //   expect(response.status).toBe(400);
    //   expect(NextResponse.json).toHaveBeenCalledWith(
    //     { message: 'Already liked this post' },
    //     { status: 400 }
    //   );
    // });


    // it('should create a like and return 201', async () => {
    //   getAuthSession.mockResolvedValue({ user: { id: 'user-id' } });
    //   prisma.like.findUnique.mockResolvedValue(null);
    
    //   const mockLike = { id: 'like-id', userId: 'user-id', postId: 1 };
    //   prisma.like.create.mockResolvedValue(mockLike);
    
    //   const { req, res } = createMocks({
    //     method: 'POST',
    //     body: { postId: 1 },
    //   });
    
    //   const response = await POST(req, res);
    
    //   expect(response.status).toBe(201);
    //   expect(NextResponse.json).toHaveBeenCalledWith(mockLike, { status: 201 });
    
    //   const jsonResponse = await response.json();
    //   expect(jsonResponse).toMatchObject({
    //     id: 'like-id',
    //     userId: 'user-id',
    //     postId: 1
    //   });
    // });

    it('should return 500 if something went wrong', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'user-id' } });
      prisma.like.findUnique.mockRejectedValue(new Error('Internal error'));

      const { req, res } = createMocks({
        method: 'POST',
        body: { postId: 1 },
      });

      const response = await POST(req, res);

      expect(response.status).toBe(500);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Something went wrong' },
        { status: 500 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Something went wrong' });
    });
  });

    it('should return 500 if something went wrong', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'user-id' } });
      prisma.like.findUnique.mockRejectedValue(new Error('Internal error'));

      const { req, res } = createMocks({
        method: 'DELETE',
        body: { postId: 1 },
      });

      const response = await DELETE(req, res);

      expect(response.status).toBe(500);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Something went wrong' },
        { status: 500 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Something went wrong' });
    });
  });