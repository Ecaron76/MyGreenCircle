import { createMocks } from 'node-mocks-http';
import { GET } from '../src/app/api/event/route';
import { getAuthSession } from '@/lib/auth';
import { PrismaClient } from '@prisma/client';
import { NextResponse } from 'next/server';

jest.mock('@prisma/client', () => {
  const mPrismaClient = {
    event: {
      findMany: jest.fn(),
    },
  };
  return { PrismaClient: jest.fn(() => mPrismaClient) };
});

jest.mock('@/lib/auth', () => ({
  getAuthSession: jest.fn(),
}));

jest.mock('next/server', () => ({
  NextResponse: {
    json: jest.fn((data, init) => {
      return {
        status: init.status,
        json: async () => data,
      };
    }),
  },
}));

const prisma = new PrismaClient();

describe('Event Retrieval API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('GET /events', () => {

    it('should return events if the user is authenticated', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });

      prisma.event.findMany.mockResolvedValue([
        {
          eventId: 1,
          title: 'Event 1',
          description: 'Description for Event 1',
          location: 'Location 1',
          startDate: new Date('2024-06-28T07:46:37.051Z'),
          endDate: new Date('2024-06-29T07:46:37.051Z'),
          status: 'active',
          createdById: 'test-user-id',
        },
        {
          eventId: 2,
          title: 'Event 2',
          description: 'Description for Event 2',
          location: 'Location 2',
          startDate: new Date('2024-07-01T07:46:37.051Z'),
          endDate: new Date('2024-07-02T07:46:37.051Z'),
          status: 'active',
          createdById: 'another-user-id',
        },
      ]);

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(200);
      expect(NextResponse.json).toHaveBeenCalledWith(
        expect.any(Array),
        { status: 200 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject([
        {
          eventId: 1,
          title: 'Event 1',
          description: 'Description for Event 1',
          location: 'Location 1',
          status: 'active',
          createdById: 'test-user-id',
        },
        {
          eventId: 2,
          title: 'Event 2',
          description: 'Description for Event 2',
          location: 'Location 2',
          status: 'active',
          createdById: 'another-user-id',
        },
      ]);
    });

    it('should return 500 if an error occurs', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });
      prisma.event.findMany.mockRejectedValue(new Error('Internal Server Error'));

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(500);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Something went wrong' },
        { status: 500 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Something went wrong' });
    });
  });
});
