import { createMocks } from 'node-mocks-http';
import { GET } from '../src/app/api/post/route';
import { getAuthSession } from '@/lib/auth';
import { PrismaClient } from '@prisma/client';
import { NextResponse } from 'next/server';

jest.mock('@prisma/client', () => {
  const mPrismaClient = {
    post: {
      findMany: jest.fn(),
    },
    join: {
      findMany: jest.fn(),
    },
  };
  return { PrismaClient: jest.fn(() => mPrismaClient) };
});

jest.mock('@/lib/auth', () => ({
  getAuthSession: jest.fn(),
}));

jest.mock('next/server', () => ({
  NextResponse: {
    json: jest.fn((data, init) => {
      return {
        status: init.status,
        json: async () => data,
      };
    }),
  },
}));

const prisma = new PrismaClient();

describe('Post Retrieval API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('GET /posts', () => {
    it('should return 403 if the user is not authenticated', async () => {
      getAuthSession.mockResolvedValue(null);

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(403);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Not Authenticated' },
        { status: 403 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Not Authenticated' });
    });

    it('should return posts if the user is authenticated', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });

      prisma.post.findMany.mockImplementation((query) => {
        if (query.where.groupId === null) {
          return [
            {
              postId: 1,
              title: 'Admin Post 1',
              content: 'Content of admin post 1',
              isVisible: true,
              userId: 'admin-user-id',
              groupId: null,
              likes: [],
              comments: [],
              user: { username: 'adminuser' },
            },
          ];
        } else {
          return [
            {
              postId: 2,
              title: 'Group Post 1',
              content: 'Content of group post 1',
              isVisible: true,
              userId: 'group-user-id',
              groupId: query.where.groupId,
              likes: [],
              comments: [],
              user: { username: 'groupuser' },
              group: { groupName: 'Test Group' },
            },
          ];
        }
      });

      prisma.join.findMany.mockResolvedValue([
        { groupId: 1 },
      ]);

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(200);
      expect(NextResponse.json).toHaveBeenCalledWith(
        expect.any(Array),
        { status: 200 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject([
        {
          postId: 1,
          title: 'Admin Post 1',
          content: 'Content of admin post 1',
          isVisible: true,
          userId: 'admin-user-id',
          groupId: null,
          user: { username: 'adminuser' },
          likesCount: 0,
          commentsCount: 0,
        },
        {
          postId: 2,
          title: 'Group Post 1',
          content: 'Content of group post 1',
          isVisible: true,
          userId: 'group-user-id',
          groupId: 1,
          user: { username: 'groupuser' },
          group: { groupName: 'Test Group' },
          likesCount: 0,
          commentsCount: 0,
        },
      ]);
    });

    it('should return 500 if an error occurs', async () => {
      getAuthSession.mockResolvedValue({ user: { id: 'test-user-id' } });
      prisma.post.findMany.mockRejectedValue(new Error('Internal Server Error'));

      const { req, res } = createMocks({
        method: 'GET',
      });

      const response = await GET(req);

      expect(response.status).toBe(500);
      expect(NextResponse.json).toHaveBeenCalledWith(
        { message: 'Something went wrong' },
        { status: 500 }
      );

      const jsonResponse = await response.json();
      expect(jsonResponse).toMatchObject({ message: 'Something went wrong' });
    });
  });
});
