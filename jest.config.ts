import nextJest from 'next/jest';

const createJestConfig = nextJest({
  dir: './',
});

const customJestConfig = {
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'babel-jest',
  },
  reporters: [
    'default',
    ['jest-junit', {
      outputDirectory: './jest-results',
      outputName: 'junit.xml',
    }],
  ],
  coverageDirectory: './coverage',
  collectCoverage: true,
};

export default createJestConfig(customJestConfig);
