// promisified fs module
import * as fs from "fs-extra";
import * as path from "path";

function getConfigurationByFile(file: string) {
  let pathToConfigFile = path.resolve("./e2e", "config", `${file}.json`);
  return fs.readJson(pathToConfigFile);
}

// plugins file
module.exports = (
  on: Cypress.PluginEvents,
  config: Cypress.PluginConfigOptions
) => {
  // accept a configFile value or use local by default
  const file = config.env.configFile || "local";

  return getConfigurationByFile(file);
};
