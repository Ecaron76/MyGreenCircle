/// <reference types="cypress" />

Cypress.Commands.add("login", (user) => {
  cy.intercept("POST", "/api/auth/callback/credentials", {
    body: { url: "http://localhost:3000/authentification" },
    statusCode: 200,
  }).as("credentials");
  cy.visit("http://localhost:3000/authentification");
  cy.get(".containerButtons > :nth-child(2)").click();
  cy.get("#email").type(user.email);
  cy.get("#password").type(user.password);
  cy.get("form > .mainBtn").click();
});

declare namespace Cypress {
  interface Chainable {
    login(user: { email: string; password: string }): Chainable<void>;
  }
}
