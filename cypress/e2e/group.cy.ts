import "cypress-file-upload";

describe("Group", () => {
  beforeEach(() => {
    cy.intercept("GET", "/api/auth/session", {
      body: {
        user: {
          email: "johndoeczcz@test.fr",
          username: "johndoe",
          admin: false,
          id: "clz6r9nyb0003104zu2gh8k9p",
          roles: [
            {
              groupId: 1,
              role: "user",
            },
          ],
        },
        expires: "2024-08-28T09:03:49.058Z",
      },
    }).as("getSession");
    cy.intercept("POST", "/api/groupe", {
      statusCode: 200,
      fixture: "post-api-group",
    }).as("createGroup");
  });

  it("Créer un groupe", () => {
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
    cy.intercept("GET", "/api/groupe", {
      statusCode: 200,
    }).as("getGroups");
    cy.intercept("GET", "/api/groupe/usergrps", {
      statusCode: 200,
    }).as("getUserGroups");
    cy.get('.flex').contains("Créer un groupe", { timeout: 15000 }).click();
    cy.fixture("post-api-group").then((group) => {
      cy.get("#groupName").type(group.groupName);
      cy.get("#groupDescription").type(group.groupDescription);
      cy.get("#groupLocation").type(group.groupLocation);
      cy.get('form.form-group').contains('Créer', { timeout: 15000 }).click();

      cy.intercept("GET", "/api/groupe/usergrps", {
        fixture: "get-api-usergrp-created",
        statusCode: 200,
      }).as("getUserGroupsCreated");
      cy.intercept("GET", "/api/groupe", {
        fixture: "get-api-usergrp-created",
        statusCode: 200,
      }).as("getGroupCreated");
      cy.get('[href="/home"]', { timeout: 15000 }).click();
      cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
      cy.wait("@getUserGroupsCreated");
      cy.wait("@getGroupCreated");
    });
  });

  it("Consulter un groupe", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getUserGroupsCreated");
  
    cy.intercept("GET", "/api/groupe/join/1/count", {
      body: {"membersCount": 1},
      statusCode: 200,
    }).as("getGroupJoinCount");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreated");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsCreated");
    cy.wait("@getGroupJoinCount");
    cy.wait("@getGroupCreated");
    cy.intercept("GET", "/api/groupe/1", {
      fixture: "get-api-group",
      statusCode: 200,
    }).as("getGroup1");
  
    cy.intercept("GET", "/api/post/1", {
      statusCode: 200,
      body: {},
    }).as("getPost1");
  
    
  
    cy.contains("Consulter", { timeout: 15000 }).should('be.visible').click();
  
    
    cy.get('.group-title').contains('Groupe test', { timeout: 15000 }).should('be.visible');
  });
  

  it("Quitter un groupe", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getUserGroupsCreated");
  
    cy.intercept("GET", "/api/groupe/join/1/count", {
      body: {"membersCount": 1},
      statusCode: 200,
    }).as("getGroupJoinCount");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreated");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsCreated");
    cy.wait("@getGroupJoinCount");
    cy.wait("@getGroupCreated");
    cy.intercept("GET", "/api/groupe/1", {
      fixture: "get-api-group",
      statusCode: 200,
    }).as("getGroup1");
    cy.intercept("GET", "/api/post/1", {
      statusCode: 200,
      body: {},
    }).as("getPost1");

    cy.contains("Consulter", { timeout: 15000 }).should('be.visible').click();
  
    
  
    
  
   
  
    cy.get('.group-title').contains('Groupe test', { timeout: 15000 }).should('be.visible');
  
    cy.intercept("DELETE", "/api/groupe/join/1", {
      statusCode: 200,
      body: {},
    }).as("leaveGroup");
  
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-empty",
      statusCode: 200,
    }).as("getUserGroupsEmpty");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreatedAgain");
  
    cy.get(".leave-btn", { timeout: 15000 }).should('be.visible').click();
  
    cy.wait("@leaveGroup");
    cy.wait("@getUserGroupsEmpty");
    cy.wait("@getGroupCreatedAgain");
  
  });
  
  
  

  it("Rejoindre un groupe", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-empty",
      statusCode: 200,
    }).as("getUserGroupsEmpty");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-groups",
      statusCode: 200,
    }).as("getGroups");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsEmpty");
    cy.wait("@getGroups");
  
    cy.intercept("POST", "/api/groupe/join/2", (req) => {
      req.reply({
        statusCode: 200,
        body: { success: true },
      });
    }).as("joinGroup");
  
    cy.intercept("GET", "/api/groupe/join/2/count", {
      statusCode: 200,
      body: { membersCount: 1 },
    }).as("getGroupJoinCount");
  
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-groups",
      statusCode: 200,
    }).as("getUserGroupsUpdated");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-groups",
      statusCode: 200,
    }).as("getGroupsUpdated");
  
    cy.contains("Rejoindre", { timeout: 15000 }).click({ force: true });
  
    cy.wait("@joinGroup").its("response.statusCode").should("eq", 200);
    cy.wait("@getUserGroupsUpdated");
    cy.wait("@getGroupsUpdated");
  
    cy.contains('Groupe à rejoindre', { timeout: 15000 }).should('be.visible');
  });
  
});
