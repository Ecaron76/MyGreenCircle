
import "cypress-file-upload";

describe("Post", () => {
  beforeEach(() => {
    cy.intercept("GET", "/api/auth/session", {
      body: {
        user: {
          email: "johndoeczcz@test.fr",
          username: "johndoe",
          admin: false,
          id: "clz6r9nyb0003104zu2gh8k9p",
          roles: [
            {
              groupId: 1,
              role: "user",
            },
          ],
        },
        expires: "2024-08-28T09:03:49.058Z",
      },
    }).as("getSession");
  });

  it("Créer un post", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getUserGroupsCreated");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreated");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsCreated");
    cy.wait("@getGroupCreated");
    cy.intercept("GET", "/api/groupe/1", {
      fixture: "get-api-group",
      statusCode: 200,
    }).as("getGroup1");
    cy.intercept("GET", "/api/post/*", {
      statusCode:200,
      body:{}

    })
  
    cy.contains("Consulter", { timeout: 15000 }).should('be.visible').click();
  
    
    cy.intercept("GET", "/api/post/1/me", {
      statusCode: 200,
      body:{}
    })

    cy.contains('Mes posts', { timeout: 15000 }).should('be.visible').click();
    cy.get('.mainBtn').contains('Ecrire un post', { timeout: 15000 }).click()
    cy.fixture("post-api-post").then((post) => {
      cy.get("#title").type(post.title);
      cy.get("#content").type(post.content);
      cy.intercept('POST','/api/post/item',{
        statusCode:200,
        fixture: "post-api-post"
      })
      cy.get("form > button").click();
      cy.intercept("GET", "/api/post/1/me", {
        statusCode: 200,
        fixture:'get-api-post-me'
      })
      cy.contains('Mes posts', { timeout: 15000 }).click()
      

      cy.get('.mb-1').contains(post.title)
    });
  });
  it("Supprimer un post", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getUserGroupsCreated");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreated");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsCreated");
    cy.wait("@getGroupCreated");
    cy.intercept("GET", "/api/groupe/1", {
      fixture: "get-api-group",
      statusCode: 200,
    }).as("getGroup1");
    cy.intercept("GET", "/api/post/*", {
      statusCode:200,
      body:{}

    })
  
    cy.contains("Consulter", { timeout: 15000 }).should('be.visible').click();
  
    
    cy.intercept("GET", "/api/post/1/me", {
      statusCode: 200,
      fixture:'get-api-post-me'
    })

    cy.contains('Mes posts', { timeout: 15000 }).should('be.visible').click();

    cy.intercept("GET", "/api/post/1/me", {
      statusCode: 200,
      fixture: 'get-api-post-me'
    }).as("getPostsMe");

    

    
  
    cy.wait(1000);
  
    cy.intercept("DELETE", "/api/post/item/1", {
      statusCode: 200,
    }).as("deletePost");
  
    cy.get('.btn-delete', { timeout: 15000 }).should('be.visible').click();
    cy.get('.btn-confirm').contains('Oui, supprimer', { timeout: 15000 }).should('be.visible').click();
  
    cy.wait("@deletePost");
    cy.get('.post-list > p').contains('No Posts found', { timeout: 15000 }).should('be.visible');
  });
  
  
  
  it("Modifier un post", () => {
    cy.intercept("GET", "/api/groupe/usergrps", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getUserGroupsCreated");
  
    cy.intercept("GET", "/api/groupe", {
      fixture: "get-api-usergrp-created",
      statusCode: 200,
    }).as("getGroupCreated");
  
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
  
    cy.get('[href="/home/groupes"]', { timeout: 15000 }).click();
  
    cy.wait("@getUserGroupsCreated");
    cy.wait("@getGroupCreated");
  
    cy.intercept("GET", "/api/groupe/*", {
      fixture: "get-api-group",
      statusCode: 200,
    }).as("getGroup1");
    cy.intercept("GET", "/api/post/*", {
      statusCode:200,
      body:{}

    })
    cy.contains("Consulter", { timeout: 15000 }).should('be.visible').click();
  
    cy.intercept("GET", "/api/post/1/me", {
      statusCode: 200,
      fixture: 'get-api-post-me'
    }).as("getPostsMe");
  
    cy.contains('Mes posts', { timeout: 15000 }).should('be.visible').click();
  
    cy.intercept("GET", "/api/post/*/me", {
      statusCode: 200,
      fixture: 'get-api-post-me'
    }).as("getPostsMe");
  
    cy.intercept("GET", "/api/post/*", {
      statusCode: 200,
      fixture: 'get-api-post-to-update'
    }).as("getPostToUpdate");
    
  
    cy.intercept("GET", "/api/post/item/*", {
      statusCode:200,
      fixture: 'get-api-post-item'
    })
    cy.wait(1000);
  
    cy.get('.btn-update', { timeout: 15000 }).should('be.visible').click();
    cy.wait(2000);
  
    cy.get('#title').clear().type("titre modifié");
    cy.get('#content').clear().type("contenu modifié");
  
    cy.intercept("PATCH", "/api/post/item/*", {
      statusCode: 200,
      body: { "title": "titre modifié", "content": "contenu modifié" }
    }).as("updatePost");
  
    cy.get('form > button').contains("Mettre à jour le post", { timeout: 15000 }).should('be.visible').click();
  
    cy.wait("@updatePost");
  
    cy.intercept("GET", "/api/post/*/me", {
      statusCode: 200,
      fixture: 'get-api-post-updated-me'
    }).as("getUpdatedPostMe");
  
    cy.intercept("GET", "/api/post/*", {
      statusCode: 200,
      fixture: 'get-api-post-updated'
    }).as("getUpdatedPost");
  
    cy.contains('Mes posts', { timeout: 15000 }).should('be.visible').click();
  
    cy.wait("@getUpdatedPostMe");
    cy.wait("@getUpdatedPost");
  
    cy.fixture('get-api-post-updated-me').then((post) => {
      cy.get('.titlepost').contains("titre modifié", { timeout: 15000 }).should('be.visible');
    });
  });
  
  

  it("Afficher post sur la page home", () => {
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
    cy.fixture('get-api-posts').then((post)=>{
      cy.intercept("GET", "/api/event/userEvents",{
        statusCode:200,

      })
      cy.intercept("GET","/api/post", {
        statusCode:200,
        fixture: 'get-api-posts'
        }
      )
      cy.get('.titlepost').contains("Post")
    })
    
    
    
  });

  it("Commenter un post", () => {
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
    });
    cy.intercept("GET","/api/post", {
      statusCode:200,
      fixture: 'get-api-posts'
      }
    )
    cy.intercept("GET","/api/comments/1", {
      statusCode:200,
      body:{}
      }
    )
    cy.get('.comment-btn').click()
    cy.get('textarea').type("Test commentaire")

    cy.intercept("POST","/api/comments/1", {
      statusCode:200,
      fixture: 'post-api-comment'
      }
    )
    
    cy.intercept("GET","/api/post", {
      statusCode:200,
      fixture: 'get-api-posts'
      }
    )
    
    
    // cy.get('.new-comment > .comment-btn').click()
    
  });

  
  
  
});
