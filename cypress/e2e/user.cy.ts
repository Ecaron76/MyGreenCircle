import "cypress-file-upload";

describe("USER: signin/signup", () => {
  it("Inscription", () => {
    cy.visit("http://localhost:3000/authentification");
    cy.fixture("post-api-signup").then((user) => {
      cy.get(".containerButtons > :nth-child(1)").click();
      cy.get("#username").type(user.username);
      cy.get("#email").type(user.email);
      cy.get("#password").type(user.password);
      cy.get("#address").type(user.address);
      cy.get("#ville").type(user.ville);
      cy.get("#CP").type(user.cp);

      cy.get("form > .mainBtn").click();

      cy.get(".success-message")
        .contains("Inscription réussie!")
        .should("be.visible");
    });
  });
  beforeEach(() => {
    cy.intercept("POST", "/api/user", {
      fixture: "post-api-signup",
    }).as("signup");

    cy.intercept("GET", "/api/event/userEvents", {
      statusCode: 200,
    }).as("getUserEvents");

    cy.intercept("GET", "/api/post", {
      statusCode: 200,
    }).as("getPosts");
    cy.intercept("GET", "/api/auth/session", {
      body: {
        user: {
          email: "johndoeczcz@test.fr",
          username: "johndoe",
          admin: false,
          id: "clz6r9nyb0003104zu2gh8k9p",
          roles: "user",
        },
        expires: "2024-08-28T09:03:49.058Z",
      },
    });
  });
  it("Connexion", () => {
    cy.visit("http://localhost:3000/authentification");
    cy.fixture("post-api-signup").then((user) => {
      cy.login(user);
      cy.get(".header-profile > p").contains(user.username);
    });
  });

  // it("Should Create Groupe", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get(".mainBtn").contains("Créer un groupe").click();
  //   cy.get("#groupName").type(groupe.name);
  //   cy.get("#groupDescription").type(groupe.description);
  //   cy.get("#groupLocation").type(groupe.location);
  //   cy.fixture("image.jpg").then((fileContent) => {
  //     cy.get('input[type="file"]#groupImage').attachFile(
  //       {
  //         fileContent,
  //         fileName: "image.jpg",
  //         mimeType: "image/jpeg",
  //         encoding: "base64",
  //       },
  //       { force: true }
  //     );
  //   });
  //   cy.get(".image-preview").attachFile("image.jpg");
  //   cy.get("form.form-group > .mainBtn").click({
  //     force: true,
  //   });
  //   cy.get(".modal-content-register", { timeout: 10000 }).should("not.exist");
  //   // cy.visit("http://localhost:3000/home/groupes");
  //   // cy.get(
  //   //   ":nth-child(3) > .groups-list > :nth-child(1) > .title-group"
  //   // ).contains(groupe.name);
  //   // cy.get(":nth-child(1) > .CTA-group > a > .groupeBtn").contains("Consulter");
  // });

  // it("Should Create Post", () => {
  //   cy.intercept("GET", "/api/groupe/join/**").as("getGroups");
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.wait("@getGroups");
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Mes posts").click();
  //   cy.get(".mainBtn").click();
  //   cy.get("#title").type(post.title);
  //   cy.get("#content").type(post.content);
  //   cy.get("form > button").click();
  //   cy.get(".postTitle").contains(post.title).should("be.visible");
  //   cy.get(".like-container > .postTrending")
  //     .contains("0 j'aime")
  //     .should("be.visible");
  // });

  // it("Should Update Post", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Mes posts").click();
  //   cy.get("a > .icon").click();
  //   // cy.get("#title").contains("have.value", post.title);
  //   // cy.get("#content").should("have.value", post.content);
  //   cy.get("#title").clear().type(postUpdated.title);
  //   cy.get("#content").clear().type(postUpdated.content);
  //   cy.get("form > button").click();
  //   cy.get(".postTitle").contains(postUpdated.title);
  // });

  // it("Should depublish Post", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Gérer les posts").click();
  //   cy.get(".unpublish-btn").click();
  //   cy.get(".modal-publish").contains("Confirmation de dépublication");
  //   cy.get(".btn-confirm").click();
  // });

  // it("Should publish Post", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Gérer les posts").click();
  //   cy.get(".publish-btn").click();
  //   cy.get(".modal-publish").contains("Confirmation de publication");
  //   cy.get(".btn-confirm").click();
  // });

  // it("Should Write A Comment In Created Post", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Mes posts").click();
  //   cy.get(".comment-container > .trending-btn").click();
  //   cy.get(".modal-comment-content").should("be.visible");
  //   cy.get("textarea").type("mon commentaire");
  //   cy.get(".new-comment > .mainBtn").click();
  //   cy.get(".comment-content").contains("mon commentaire");
  // });

  // it("Should Create Event", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get("a > .groupeBtn").click();
  //   cy.contains("a", "Créer un évènement").click();
  //   cy.get("#title").type(event.title);
  //   cy.get("#description").type(event.description);
  //   cy.get("#location").type(event.location);
  //   cy.get("#startDate").type(event.startDate);
  //   cy.get("#endDate").type(event.endDate);
  //   cy.get("#status").type(event.status);
  //   cy.get("#limitSubscriptionDate").type(event.limitDate);
  //   cy.get("#address").type(event.address);
  //   cy.get("#ville").type(event.ville);
  //   cy.get("#CP").type(event.cp);
  //   cy.get("#latitude").type(event.latitude);
  //   cy.get("#longitude").type(event.longitude);
  //   cy.get(".mainBtn").click();
  //   cy.get('[href="/home/events"]').click();
  //   cy.get(".eventTitle").contains(event.title);
  // });

  // it("Delete Event", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/events"]').click();
  //   cy.get(".mainBtn").click();
  //   cy.get(".content-page > p").contains("No events found.");
  // });

  // it("Should Join Groupe", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/groupes"]').click();
  //   cy.get(
  //     ":nth-child(1) > .CTA-group > .groupeBtn-container > .groupeBtn"
  //   ).click();
  //   let clickedButton;
  //   cy.get(".groups-list > :nth-child(2) > .title-group").then(
  //     ($groupTitle) => {
  //       if ($groupTitle.text().trim() !== groupe.name) {
  //         clickedButton = cy.get(":nth-child(2) > .CTA-group > a > .groupeBtn");
  //         clickedButton.click();
  //       } else {
  //         clickedButton = cy.get(":nth-child(1) > .CTA-group > a > .groupeBtn");
  //         clickedButton.click();
  //       }
  //     }
  //   );
  //   cy.get(".leave-btn").contains("Quitter le groupe").should("be.visible");
  // });

  // it("Participate in event", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get('[href="/home/events"]').click();
  //   cy.get(":nth-child(1) > .eventCard > .eventButtons > .participBtn").click();
  //   cy.get(".participantBadge")
  //     .contains("Vous participez")
  //     .should("be.visible");
  // });

  // it("Deconnexion", () => {
  //   cy.visit("http://localhost:3000/home");
  //   cy.contains(user.username).should("be.visible");
  //   cy.get(".signout-btn").click();
  //   cy.get("main").contains("Vous devez être connecté pour voir cette page.");
  // });

  // it("Connect as Admin", () => {
  //   cy.visit("http://localhost:3000/authentification");
  //   cy.get(".containerButtons > :nth-child(2)").click();
  //   cy.get("#email").type("admin@admin.fr");
  //   cy.get("#password").type("admin");
  //   cy.get("form > .mainBtn").click();
  //   cy.contains("admin").should("be.visible");
  //   cy.getCookies().then((cookies) => {
  //     const authCookies = cookies.map((cookie) => ({
  //       name: cookie.name,
  //       value: cookie.value,
  //     }));
  //     Cypress.env("authCookies", authCookies);
  //   });
  // });

  // beforeEach(() => {
  //   const authCookies = Cypress.env("authCookies");
  //   if (authCookies) {
  //     authCookies.forEach((cookie: any) => {
  //       cy.setCookie(cookie.name, cookie.value);
  //     });
  //   }
  // });

  // it("should admin delete user", () => {
  //   cy.visit("http://localhost:3000/dashboard");
  //   cy.contains("admin").should("be.visible");
  //   cy.get(".MuiList-root > :nth-child(2) > .MuiBox-root").click();
  //   cy.get(".MuiDataGrid-row")
  //     .its("length")
  //     .then((initialRowCount) => {
  //       cy.get(".MuiList-root > :nth-child(2) > .MuiBox-root").click();
  //       cy.get(
  //         ".MuiDataGrid-row--lastVisible > .MuiDataGrid-cell--flex > .MuiDataGrid-actionsCell"
  //       ).click();
  //       cy.get("#alert-dialog-title")
  //         .contains("Confirmez la suppression")
  //         .should("be.visible");
  //       cy.get(".MuiDialogActions-root > .MuiButton-text").click();
  //       cy.wait(4000);
  //       cy.reload();
  //       cy.get(".MuiDataGrid-row")
  //         .its("length")
  //         .should("equal", initialRowCount - 1);
  //     });
  // });

  // it("should admin delete post", () => {
  //   cy.visit("http://localhost:3000/dashboard");
  //   cy.get(":nth-child(4) > .MuiBox-root").click();
  //   cy.contains("admin").should("be.visible");
  //   cy.get(
  //     ".MuiDataGrid-row--lastVisible > .MuiDataGrid-cell--flex > .MuiDataGrid-actionsCell"
  //   ).click();
  //   cy.get("#alert-dialog-title")
  //     .contains("Confirmez la suppression")
  //     .should("be.visible");
  //   cy.get(".MuiDialogActions-root > .MuiButton-text").click();
  //   cy.reload();
  // });

  // it("should admin delete group", () => {
  //   cy.visit("http://localhost:3000/dashboard");
  //   cy.get(":nth-child(4) > .MuiBox-root").click();
  //   cy.contains("admin").should("be.visible");
  //   cy.get(
  //     ".MuiDataGrid-row--lastVisible > .MuiDataGrid-cell--flex > .MuiDataGrid-actionsCell"
  //   ).click();
  //   cy.get("#alert-dialog-title")
  //     .contains("Confirmez la suppression")
  //     .should("be.visible");
  //   cy.get(".MuiDialogActions-root > .MuiButton-text").click();
  //   cy.reload();
  // });
});
