
'use client';

import Header from "@/components/UI/Header/Header";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import './SingleGroupePage.css';
import { useRouter } from "next/navigation";
import PostCard from "@/components/UI/PostCard/PostCard";
import Link from "next/link";
import CreateEventModal from "@/components/GroupePage/CreateEventModal/CreateEventModal";
import CommentModal from "@/components/UI/CommentModal/CommentModal";

type SingleGroupePageProps = {
  params: {
    groupId: number;
  }
}

interface UserRole {
  groupId: number;
  role: string;
}

interface GroupDetails {
  groupId: number;
  groupName: string;
  groupDescription: string;
  groupLocation: string;
}

interface Post {
    postId: number;
    title: string;
    content: string;
    picture?: string;
    user: {
        username: string;
    };
    likesCount: number;
    commentsCount: number;
  };


const SingleGroupePage = ({ params }: SingleGroupePageProps) => {
  const { data: session } = useSession();
  const router = useRouter();
  const { groupId } = params;

  const [groupDetails, setGroupDetails] = useState<GroupDetails>();
  const [allGroupPosts, setAllGroupPosts] = useState<Post[]>([]);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [isCommentModalOpen, setIsCommentModalOpen] = useState<boolean>(false);
  const [selectedPostComments, setSelectedPostComments] = useState<number | null>(null);

  const role = session?.user.roles?.find((r: UserRole) => r.groupId === Number(groupId))?.role;

  const fetchGroupDetails = async () => {
    if (!groupId) return;
    setIsLoading(true);
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/groupe/${groupId}`);
      if (!response.ok) throw new Error('Failed to fetch group details');
      const data = await response.json();
      setGroupDetails(data.group);
    } catch (error: unknown) {
      if (error instanceof Error) {
        setError(error.message);
      } else {
        setError(String(error));
      }
    } finally {
      setIsLoading(false);
    }
  };

  const fetchAllGroupPost = async () => {
    if (!groupId) return;
    setIsLoading(true);
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/post/${groupId}`);
      if (!response.ok) throw new Error('Failed to fetch group posts');
      const dataPosts = await response.json();
      setAllGroupPosts(dataPosts);
    } catch (error: unknown) {
      if (error instanceof Error) {
        setError(error.message);
      } else {
        setError(String(error));
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleLeaveGroup = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/groupe/join/${groupId}`, {
        method: 'DELETE',
      });
      if (!response.ok) throw new Error('Failed to leave group');
      alert('You have left the group successfully');
      router.push('/home/groupes');
    } catch (error) {
      console.error('Error leaving group:', error);
    }
  };

  const handleCommentClick = (postId: number) => {
    setSelectedPostComments(postId);
    setIsCommentModalOpen(true);
  };
  const getPostTitle = (postId: number): string => {
    const post = allGroupPosts.find(post => post.postId === postId) || allGroupPosts.find(post => post.postId === postId);
    return post ? post.title : '';
  };

  useEffect(() => {
    fetchGroupDetails();
    fetchAllGroupPost();
  }, [groupId]);

  if (!session?.user) {
    return <div>Not authenticated</div>;
  }

  return (
    <main>
      <Header username={session.user.username} admin={session.user.admin} />
      <div className="group-details-container">
        {isLoading ? (
          <div className="loading-circle">
            <svg className="spinner" viewBox="0 0 50 50">
              <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
            </svg>
          </div>
        ) : error ? (
          <p>Error: {error}</p>
        ) : groupDetails ? (
          <div>
            <h1 className="group-title">{groupDetails.groupName}</h1>
            <p>{groupDetails.groupDescription}</p>
            <p>{groupDetails.groupLocation}</p>
            {role !== 'admin' && (
              <button className="leave-btn" onClick={handleLeaveGroup}>Quitter le groupe</button>
            )}
          </div>
        ) : (
          <p>No group details found</p>
        )}
      </div>
      <nav className="group-navbar bg-white shadow-md rounded-lg p-4 flex justify-around items-center space-x-4">
  <Link href={`/home/groupes/${groupId}/myposts`} passHref>
    <div className="text-mainGreen font-semibold hover:text-white hover:bg-mainGreen transition-colors duration-300 px-4 py-2 rounded-md cursor-pointer border border-mainGreen bg-white">
      Mes posts
    </div>
  </Link>
  {role === 'admin' && (
    <Link href={`/home/groupes/${groupId}/posts-manager`} passHref>
      <div className="text-mainGreen font-semibold hover:text-white hover:bg-mainGreen transition-colors duration-300 px-4 py-2 rounded-md cursor-pointer border border-mainGreen bg-white">
        Gérer les posts
      </div>
    </Link>
  )}
  {role == 'admin' && (
    <div onClick={() => setIsModalOpen(true)} className="text-mainGreen font-semibold hover:text-white hover:bg-mainGreen transition-colors duration-300 px-4 py-2 rounded-md cursor-pointer border border-mainGreen bg-white">
      Créer un évènement
    </div>
  )}
</nav>



      <h2 className="title-section">Publications</h2>
      <div className="post-list">
        {isLoading ? (
          <div className="loading-circle">
            <svg className="spinner" viewBox="0 0 50 50">
              <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
            </svg>
          </div>
        ) : error ? (
          <p>Error: {error}</p>
        ) : allGroupPosts.length > 0 ? (
          allGroupPosts.map((post: Post) => (
            <PostCard
                key={post.postId}
                postId={post.postId}
                title={post.title}
                content={post.content}
                picture={post.picture}
                group
                author={post.user.username}
                nbComment={post.commentsCount}
                nbLike={post.likesCount}
                onCommentClick={handleCommentClick}
                />
          ))
        ) : (
          <p>No Posts found</p>
        )}
      </div>
      {isModalOpen && (
        <CreateEventModal
          groupId={groupId}
          onClose={() => setIsModalOpen(false)}
          onSuccess={() => {
            setIsModalOpen(false);
            fetchAllGroupPost();
          }}
        />
      )}
      {isCommentModalOpen && selectedPostComments !== null && (
            <CommentModal postId={selectedPostComments} onClose={() => setIsCommentModalOpen(false)} postTitle={getPostTitle(selectedPostComments)}/>

        )}
    </main>
  );
};

export default SingleGroupePage;
