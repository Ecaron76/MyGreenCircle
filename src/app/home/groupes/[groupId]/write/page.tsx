'use client'

import { useEffect, useState } from "react";
import Header from "@/components/UI/Header/Header";
import './write.css'
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";


type WritePostPageProps = {
    params: {
        groupId: number;
    };
};

interface GroupDetails {
    groupId: number;
    groupName: string;
    groupDescription: string;
    groupLocation: string;
};

const WritePostPage = ({ params }: WritePostPageProps) => {
    const { data: session } = useSession();
    const router = useRouter();
    const { groupId } = params;

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('');
    const [file, setFile] = useState<File>()
    const [imageObjectUrl, setImageObjectUrl] = useState<string | null>(null)
    const [imageUploadError, setImageUploadError] = useState<string>('');
    const [groupDetails, setGroupDetails] = useState<GroupDetails>();

    const uploadImage = async () => {
        if (!file) return null;

        const imageData = new FormData();
        imageData.append('file', file);
        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/upload`, {
                method: 'POST',
                body: imageData,
            });

            if (!response.ok) {
                throw new Error('Failed to upload image');
            }

            const data = await response.json();
            return data.url;
        } catch (error) {
            console.error('Error in uploading image', error);
            setImageUploadError('Failed to upload image, wrong format. Please provide a jpeg, jpg or png file.');
            return null;
        }
    }

    const createPost = async (e: React.FormEvent) => {
        e.preventDefault();
        setIsLoading(true);

        try {
            let imageUrl = null;
            if (file) {
                imageUrl = await uploadImage();
                if (!imageUrl) {
                    throw new Error('Image upload failed');
                }
            }
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/post/item`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ title, content, groupId: Number(groupId), picture: imageUrl }),
            });

            if (!response.ok) {
                throw new Error('Failed to create post');
            }

            const data = await response.json();
            router.push(`/home/groupes/${groupId}`);
        } catch (error) {
            setError('Failed to create post');
        } finally {
            setIsLoading(false);
        }
    };

    const fetchGroupDetails = async () => {
        if (!groupId) return;

        setIsLoading(true);
        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/groupe/${groupId}`);
            if (!response.ok) throw new Error('Failed to fetch group details');

            const data = await response.json();
            setGroupDetails(data.group);
        } catch (error) {
            setError('Failed to fetch group details');
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        fetchGroupDetails()
    }, [groupId]);

    if (session?.user) {
        return (
            <main>
                <Header username={session.user.username} admin={session.user.admin}/>
                <div className="group-details-container">
                    {isLoading ? (
                        <div className="loading-circle">
                            <svg className="spinner" viewBox="0 0 50 50">
                                <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
                            </svg>
                        </div>
                    ) : error ? (
                        <p>Error: {error}</p>
                    ) : groupDetails ? (
                        <div>
                            <h1 className="group-title">{groupDetails.groupName}</h1>
                            <p>{groupDetails.groupDescription}</p>
                            <p>{groupDetails.groupLocation}</p>
                        </div>
                    ) : (
                        <p>No group details found</p>
                    )}
                </div>
                <form onSubmit={createPost} className="space-y-6 bg-white p-8 rounded-lg shadow-md">
  <div className="form-group">
    <label htmlFor="title" className="block text-sm font-medium text-gray-700">Titre:</label>
    <input
      type="text"
      id="title"
      value={title}
      onChange={(e) => setTitle(e.target.value)}
      required
      className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
    />
  </div>
  <div className="form-group">
    <label htmlFor="content" className="block text-sm font-medium text-gray-700">Contenu:</label>
    <textarea
      id="content"
      value={content}
      onChange={(e) => setContent(e.target.value)}
      required
      className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
    />
  </div>
  {/* <div className="form-group">
    <input
      type="file"
      id="picture"
      onChange={(e) => {
        if (e.target.files) {
          setFile(e.target.files[0]);
          setImageObjectUrl(URL.createObjectURL(e.target.files[0]));
          setImageUploadError(''); // Clear previous error
        }
      }}
      style={{ display: 'none' }}
    />
    <label 
      htmlFor="picture" 
      className="cursor-pointer bg-indigo-600 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center"
    >
      Choisir une image
    </label>
    {imageObjectUrl && <img src={imageObjectUrl} alt="Post Preview" className="mt-4 max-w-full h-auto rounded" />}
    {imageUploadError && <p className="text-red-500 text-sm mt-2">{imageUploadError}</p>}
  </div> */}
  <button 
    type="submit" 
    disabled={isLoading} 
    className={`w-full inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white ${isLoading ? 'bg-gray-400' : 'bg-mainGreen hover:bg-mainGreen-700'} focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500`}
  >
    {isLoading ? 'Création...' : 'Créer le post'}
  </button>
</form>

            </main>
        );
    } else {
        return <p>Please sign in to create a post.</p>
    }
}

export default WritePostPage;
