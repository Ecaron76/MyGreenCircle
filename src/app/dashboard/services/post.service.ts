import { Post } from "../types/types";


export const getAllPosts = async (): Promise<Post[]> => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/post`);
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data: Post[] = await response.json();
    return data;
  } catch (error) {
    console.error("Failed to fetch post data:", error);
    throw error;
  }
};

export const createPost = async (postData: any): Promise<Post> => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/post/item`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postData),
      credentials: "include",
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data: Post = await response.json();
    return data;
  } catch (error) {
    console.error("Failed to create post:", error);
    throw error;
  }
};

export const PostVisibility = async (
  postId: number,
  isVisible: boolean
): Promise<void> => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/post/item/${postId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ isVisible }),
    });

    if (!response.ok) {
      throw new Error("Failed to toggle post visibility");
    }
  } catch (error) {
    console.error("Failed to toggle post visibility", error);
    throw error;
  }
};

export const getCommentsByPostId = async (postId: number) => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/comments/${postId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    });

    if (!response.ok) {
      throw new Error(`Error: ${response.statusText}`);
    }

    const data = await response.json();
    return data.comments;
  } catch (error) {
    console.error("Failed to fetch comments:", error);
    throw error;
  }
};
