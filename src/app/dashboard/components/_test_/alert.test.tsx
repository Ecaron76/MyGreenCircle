import React from "react";
import { render, screen, act } from "@testing-library/react";
import "@testing-library/jest-dom";
import AlertComponents from "../Alert/Page";

interface MockAlertComponentProps {
  title: string;
}

const MockAlertComponent: React.FC<MockAlertComponentProps> = ({ title }) => {
  return <AlertComponents title={title} />;
};

describe("AlertComponents", () => {
  jest.useFakeTimers();

  it("renders the alert initially", () => {
    render(<MockAlertComponent title="Test Successful!" />);
    expect(screen.getByText("Test Successful")).toBeInTheDocument();
  });

  it("removes the alert from the DOM after 2000 milliseconds", () => {
    render(<MockAlertComponent title="Test Successful!" />);
    expect(screen.getByText("Test Successful")).toBeInTheDocument();

    act(() => {
      jest.advanceTimersByTime(2000);
    });
    expect(screen.queryByText("Test Successful")).not.toBeInTheDocument();
  });
});

afterAll(() => {
  jest.useRealTimers();
});
