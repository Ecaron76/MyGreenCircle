import React from "react";
import { render, screen } from "@testing-library/react";
import DataGridComponent from "../dataGrid/Page";
import "@testing-library/jest-dom";

interface DataRow {
  id: number;
  firstName: string;
  lastName: string;
}

interface MockDataGridComponentProps {
  rows: DataRow[];
  loading?: boolean;
}

describe("DataGridComponent", () => {
  const mockColumns = [
    { field: "id", headerName: "ID", width: 90 },
    { field: "firstName", headerName: "First name", width: 150 },
    { field: "lastName", headerName: "Last name", width: 150 },
  ];
  const mockRows = [
    { id: 1, firstName: "John", lastName: "Doe" },
    { id: 2, firstName: "Jane", lastName: "Doe" },
  ];
  const emptyRows: DataRow[] = [];

  const MockDataGridComponent: React.FC<MockDataGridComponentProps> = ({
    rows,
    loading = false,
  }) => {
    return (
      <DataGridComponent
        rows={rows}
        columns={mockColumns}
        getRowId={(row) => row.id}
        loading={loading}
        identifier="test-grid"
      />
    );
  };

  it("renders DataGrid with rows and columns", () => {
    render(<MockDataGridComponent rows={mockRows} />);
    expect(screen.getByText("John")).toBeInTheDocument();
    expect(screen.getByText("Jane")).toBeInTheDocument();
  });

  it("should display the correct number of rows", () => {
    render(<MockDataGridComponent rows={mockRows} />);
    const rows = screen.getAllByRole("row");
    expect(rows).toHaveLength(mockRows.length + 1);
  });

  it("should display 'Aucun résultat disponible' when there are no rows", () => {
    render(<MockDataGridComponent rows={emptyRows} />);
    expect(screen.getByText("Aucun résultat disponible")).toBeInTheDocument();
  });
});
