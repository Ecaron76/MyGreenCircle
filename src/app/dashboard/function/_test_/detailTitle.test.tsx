import { calculateDetailTitle } from "../detailTitle";

describe("calculateDetailTitle Function Tests", () => {
  test.each(["apple", "orange", "umbrella", "elephant", "inbox", "éclat"])(
    'should return "de l’" for words starting with a vowel or "h" or "é" (%s)',
    (word) => {
      expect(calculateDetailTitle(word)).toBe("de l’");
    }
  );

  test('should handle strings starting with non-alphabet characters by returning "du "', () => {
    expect(calculateDetailTitle("Post")).toBe("du ");
  });
});
