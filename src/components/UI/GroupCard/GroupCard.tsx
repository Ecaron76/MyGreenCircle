import React from 'react';
import Image from 'next/image';
import './GroupCard.css';
import Link from 'next/link';

type GroupCardProps = {
  title: string;
  description?: string;
  nbMember: number;
  image?: string;
  myGroup?: boolean;
  groupId: number;
  group?: boolean;
  refreshGroups?: () => void;
};

const GroupCard: React.FC<GroupCardProps> = ({ title, description, nbMember, myGroup, groupId, refreshGroups, image, group }) => {
  const defaultImage = '/assets/images/groupe.png';

  const handleJoinGroup = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/groupe/join/${groupId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Joined group successfully:', data);
        refreshGroups && refreshGroups();
      } else {
        console.error('Failed to join group:', response.statusText);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="flex flex-col w-full max-w-lg p-1 space-y-6 overflow-hidden rounded-lg custom-shadow dark:bg-gray-50 dark:text-gray-800">
    <Image alt={title} src={image || defaultImage} width={300} height={200} className="object-fill w-full mb-4 h-60 sm:h-74 dark:bg-gray-500"/>
    <div className="flex flex-col items-center justify-center p-6 space-y-8 text-center">
        <div className="space-y-1">
            <h2 className="text-3xl font-semibold tracking-wide">{title}</h2>
            <p className="dark:text-gray-800">{description}</p>
            <div className='font-semibold'>{nbMember} membres</div>   
        </div>
        
        {myGroup ? (
            <Link href={`/home/groupes/${groupId}`}>
                <button className="w-full flex items-center justify-center px-8 py-2 font-semibold rounded bg-mainGreen text-gray-100">Consulter</button>
            </Link>
        ) : (
            <div className="groupeBtn-container w-full flex items-center justify-center">
                <button className="w-1/2 flex items-center justify-center px-8 py-2 font-semibold rounded bg-mainGreen text-gray-100" onClick={handleJoinGroup}>Rejoindre</button>
            </div>
        )}
    </div>
  </div>


    

  );
};

export default GroupCard;
